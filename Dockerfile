FROM gradle:latest
ADD ./sourse/build/libs/tchallenge-servise.jar /app/app.jar
CMD ["java", ".jar", "/app/app.jar"]
